//index.js모듈파일
// a.js와 b.js 모듈안의 요소 사용
// 노출된 요소를 import 해서 사용한다.

//export 노출
import { Person, fun2 } from './a.js' //export는 중괄호를 넣어 import한다.
import { Person as ppp } from './a.js' //as로 별칭을 넣어줄수있다.
import { fun1 } from './b.js'

//export default 노출
import fun3 from './c.js'; //export default 는 중괄호 없이 import한다.
import defaultExport from './c.js'; //함수이름을 모를때 defaultExport
import {default as xxx} from './c.js'; //별칭 부여

var p = new Person();
p.info();

var p = new ppp();
p.info();

fun2();

fun1();

fun3();

defaultExport();
xxx();