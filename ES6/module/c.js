//c.js모듈파일
// 외부로 노출시켜야한다. ==> export export는 여러개 노출 가능
// 외부로 노출시켜야한다. ==> default export  default export는 반드시 한번만 사용 가능
export default function fun3() {
    console.log("fun3");
}