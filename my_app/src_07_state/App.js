import logo from './logo.svg';
import './App.css';
import ChildCompnent from './child/ChildCompnent';

function App() {
  return (
    <div className="App">
      <h1>부모 컴포넌트</h1>
      <ChildCompnent />
    </div>
  );
}

export default App;
