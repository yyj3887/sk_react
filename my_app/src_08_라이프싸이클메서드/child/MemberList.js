import React, { Component } from 'react';
import Member from './Member';

class MemberList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            memberData: [],
            username:"",
            age:"",
            address:"",
        }
        console.log("MemberList.MemberList");
    }
    ///////////////////////////////////////////////////////////

    // static getDerivedStateFromProps(nextProps, prevState) {
    //     console.log("MemberList.getDerivedStateFromProps");
    //     console.log("MemberList.getDerivedStateFromProps.nextProps",nextProps);
    //     console.log("MemberList.getDerivedStateFromProps.prevState",prevState);        
    //     return null;
    // }
    
    componentDidMount() {
        console.log("MemberList.componentDidMount");
    }
    /////////////////////////////////////////////////////////////

    ////업데이터 관련 메서드/////////////////////////////////////
    shouldComponentUpdate(prevProps,prevState) {
        console.log("MemberList.shouldComponentUpdate");
        console.log("MemberList.shouldComponentUpdate.prevProps",prevProps);
        console.log("MemberList.shouldComponentUpdate.prevState",prevState);    
        //조건지정해서 재 랜더링 여부 설정    ==> 성능이슈 관련
        return true;
    }
    // getSnapshotBeforeUpdate(prevProps,prevState) {
    //     console.log("MemberList.getSnapshotBeforeUpdate");
    //     console.log("MemberList.getSnapshotBeforeUpdate.prevProps",prevProps);
    //     console.log("MemberList.getSnapshotBeforeUpdate.prevState",prevState);    
    //     return { 'default':1000}
    // }
    // componentDidUpdate(prevProps,prevState,snapshot) {
    //     console.log("MemberList.componentDidUpdate");
    //     console.log("MemberList.componentDidUpdate.prevProps",prevProps);
    //     console.log("MemberList.componentDidUpdate.prevState",prevState);  
    //     console.log("MemberList.componentDidUpdate.prevState",snapshot);  
    // }

    // componentWillUnmount() {
    //     console.log("MemberList.componentWillUnmount");
    // }

    /////////////////////////////////////////////////
    render() {        
        console.log("MemberList.render");
        return (
            <div>
                <form onSubmit={this.HandleSubmit}>
                    이름:<input type="text" name="username" onChange={ this.HandleChange } value={ this.state.username }/><br/>
                    나이:<input type="text" name="age" onChange={ this.HandleChange } value={ this.state.age } /><br/>
                    주소:<input type="text" name="address" onChange={ this.HandleChange } value={ this.state.address } /><br/>
                    <button type="submit">저장</button>
                </form>
                {
                    this.state.memberData.map( (row,index) => {
                        return <Member 
                        key={index}
                        row={row}
                        index={index}
                        handleDel={this.handleDel}
                        />
                    })
                }                
            </div>
        );
    }

    HandleSubmit = (e) => {
        e.preventDefault();

        let user = {
            username: this.state.username,
            age: this.state.age,
            address: this.state.address,
        }

        this.setState({
            memberData:[...this.state.memberData,user]
        })

    }

    HandleChange = (e) => {
        //동적 키 생성
        let nextState = {};
        nextState[e.target.name]=e.target.value;
        this.setState(nextState);
    }

    //삭제 기능
    handleDel = (idx) => {              
        this.setState( {
            memberData : this.state.memberData.filter( (v,i) => {                              
                return i != idx 
            })
        })        
    }
}

export default MemberList;