import React, { Component } from 'react';

class Member extends Component {

    constructor(props) {
        super(props);
        console.log("Member.constructor");
    }
    
    ///////////////////////////////////////////////////////////

        // static getDerivedStateFromProps(nextProps, prevState) {
        //     console.log("Member.getDerivedStateFromProps");
        //     console.log("Member.getDerivedStateFromProps.Props",nextProps);
        //     console.log("Member.getDerivedStateFromProps.prevState",prevState);        
        //     return null;
        // }
        
    componentDidMount() {
         console.log("Member.componentDidMount");
    }
        ////업데이터 관련 메서드/////////////////////////////////////
        shouldComponentUpdate(prevProps,prevState) {
            console.log("Member.shouldComponentUpdate");
            console.log("Member.shouldComponentUpdate.prevProps",prevProps);
            console.log("Member.shouldComponentUpdate.prevState",prevState);        
            return true;
        }
        // getSnapshotBeforeUpdate(prevProps,prevState) {
        //     console.log("Member.getSnapshotBeforeUpdate");
        //     console.log("Member.getSnapshotBeforeUpdate.prevProps",prevProps);
        //     console.log("Member.getSnapshotBeforeUpdate.prevState",prevState);    
        //     return { 'default':1000}
        // }
        // componentDidUpdate(prevProps,prevState,snapshot) {
        //     console.log("Member.componentDidUpdate");
        //     console.log("Member.componentDidUpdate.prevProps",prevProps);
        //     console.log("Member.componentDidUpdate.prevState",prevState);  
        //     console.log("Member.componentDidUpdate.snapshot",snapshot);  
        // }
    
        componentWillUnmount() {
            console.log("Member.componentWillUnmount");
        }
    /////////////////////////////////////////////////
    render() {
        console.log("Member.render");
        const {row , index , handleDel} = this.props;
        return (
            <div>
                 <table border="1">
                    <thead>
                        <td>{row.username}</td>
                        <td>{row.age}</td>
                        <td>{row.address}</td>
                    </thead>           
                </table>              
                <button onClick={ () => { handleDel(index) } }>삭제</button>  
            </div>
        );
    }
}

export default Member;