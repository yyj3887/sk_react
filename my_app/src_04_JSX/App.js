import './App.css';
import ChildCompoent from './child/ChildCompoent';
import ChildComponent2 from './child/ChildComponent2';
import ChildComponent3 from './child/ChildComponent3';
import ChildComponent4 from './child/ChildComponent4';
import ChildComponent5 from './child/ChildComponent5';
import ChildComponent6 from './child/ChildComponent6';
import ChildComponent7 from './child/ChildComponent7';
import ChildComponent8 from './child/ChildComponent8';

function App() {
  return (
    <div>
      <ChildCompoent/>      
      <ChildComponent2/>
      <ChildComponent3/>
      <ChildComponent4/>
      <ChildComponent5/>
      <ChildComponent6/>
      <ChildComponent7/>
      <ChildComponent8/>
    </div>
  );
}

export default App;
