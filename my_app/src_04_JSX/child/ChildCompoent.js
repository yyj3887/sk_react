import React, { Component } from 'react';

class ChildCompoent extends Component {
    render() {
        return (
            <div>
                <h2> 1) JSX: 모든 태그는 반드시 종료태그가 필수이다.</h2>
                <br/>                
            </div>
        );
    }
}

export default ChildCompoent;
