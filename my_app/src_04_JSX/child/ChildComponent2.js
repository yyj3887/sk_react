import React, { Component } from 'react';

class ChildComponent2 extends Component {
    render() {
        return (
            <>
                <h2>2) JSX: 반드시 단 하나의 Root태그가 필요하다. </h2>
                <p>Hello</p>
            </>
        );
    }
}

export default ChildComponent2;