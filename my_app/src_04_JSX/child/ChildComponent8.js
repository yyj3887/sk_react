import React, { Component } from 'react';

class ChildComponent8 extends Component {
    render() {
        //일반적인 주석
        /* 
            멀티라인 주석
         */
        const my_style = {
            fontSize:'20px',
            backgroundColor:'blue'
        }

        return (
            <div>
                {/* 이것은 JSK 주석입니다. 컨트롤 + / */}
                <h2>8) JSX: 스타일 지정시 style 속성값은 JSON형태로 사용 + CSS송성명은 Camel표기법</h2>
                <p style={my_style}>Hello</p>
            </div>
        );
    }
}

export default ChildComponent8;