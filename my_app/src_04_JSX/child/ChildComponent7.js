import React, { Component } from 'react';
import './child.css'
class ChildComponent7 extends Component {
    render() {
        return (
            <div>
                <h2>7) JSX: 스타일 지정시 Class 속성 대신에 ClassName 속성을 사용해야 된다.</h2>
                <p className='xyz'>Hello</p>
            </div>
        );
    }
}

export default ChildComponent7;