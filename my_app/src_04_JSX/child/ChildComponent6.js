import React, { Component } from 'react';

class ChildComponent6 extends Component {
    render() {
        return (
            <div>
                <h2>6) JSX: 이벤트 처리시 핸들러명은 Camel 표기법 필수이다.</h2>
                <button onClick={ () => console.log("OK!!Event") }>OK</button>
            </div>
        );
    }
}

export default ChildComponent6;