import React, { Component } from 'react';

class ChildComponent5 extends Component {
    render() {
        let attr = {
            href: "http://www.google.com",
            target: "_blank"
        }
        return (
            <div>
                <h2>5) JSX: Spread 연산자 사용 가능하다.</h2>
                <a href="http://www.google.com" target="_blank">구글1</a>
                <br></br>
                <a {...attr}>구글2</a>
            </div>
        );
    }
}

export default ChildComponent5;