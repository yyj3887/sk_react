import React, { Component } from 'react';

class ChildComponent3 extends Component {
    render() {
        let mesg = "Hello";
        return (
            <div>
                <h2> 3) JSX: 임의의 변수값을 출력할 때는 인터폴레이션을 사용한다.</h2>
                메세지:{mesg}
            </div>
        );
    }
}

export default ChildComponent3;