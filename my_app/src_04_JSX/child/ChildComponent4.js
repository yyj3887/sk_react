import React, { Component } from 'react';

class ChildComponent4 extends Component {
    render() {

        let names = ["홍길동","이순신","강감찬","양영재"]

        return (
            <div>
                <h2>4) JSX : 자바코드 작성시 인터폴레이션 필수이다.</h2>
                <ul>
                    {
                        names.map((value,index) => {
                            return <li key={index}>{value}</li>
                        })
                    }
                </ul>
            </div>
        );
    }
}

export default ChildComponent4;