import React, { Component } from 'react';

class Person extends Component {
    render() {        
        const { row, index } = this.props;
        return (
            <tr>
                <td>{index+1}</td>
                <td>{row.name}</td>                
                <td>{row.age}</td>                
            </tr>
        );
    }
}

export default Person;