import React, { Component } from 'react';
import Person from './Person';

class PersonList extends Component {    
    render() {
        const {persons} = this.props;
        return (
            <div>
                <table border="1">
                    <thead>
                        <td>번호</td>
                        <td>이름</td>
                        <td>나이</td>
                    </thead>
                    <tbody>
                        {
                            persons.map((value,index) => {
                                return <Person row={value} index={index} key={index}/>
                            })
                        }
                    </tbody>
                </table>
            </div>
        );
    }
}

export default PersonList;