//React의 컴포넌트

import React, { Component } from 'react';

class ChildCompnent extends Component {

    constructor(props) {
        super(props);

        //ref값 (마킹값) 초기화
        this.idInput = React.createRef();
        this.pwInput = React.createRef();
      }

      handleSubmit = (e) => {
        e.preventDefault(); //화면갱신을 막는다.

        //ref 값 얻기
        console.log(this.idInput.current.value);
        console.log(this.pwInput.current.value);

        this.idInput.current.value = '';
        this.pwInput.current.value = '';    

        this.idInput.current.focus();
      }

    render() {
        return (
            <div>                
                <form onSubmit={this.handleSubmit}>
                    <label>
                          userid: <input type="text" ref={this.idInput}/> <br/>
                          pw:     <input type="text" ref={this.pwInput}/> <br/>
                    </label>
                    <input type="submit" value="Submit" />
                </form>
            </div>
        );
    }
}

export default ChildCompnent;