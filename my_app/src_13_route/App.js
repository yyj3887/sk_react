import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Navi from "./nav/Navi";
import About from "./Pages/About";
import Dashboard from "./Pages/Dashboard";
import Home from "./Pages/Home";

function App() {
  return (
    <div>
      <Router>
        <Navi />
        <hr />
        <Route exact path="/" component={Home} />
        <Route path="/about" component={About} />
        <Route path="/dashoborad/:id" component={Dashboard} />
      </Router>
    </div>
  );
}

export default App;
