import React, { Component } from "react";
import { NavLink } from "react-router-dom";
class Navi extends Component {
  render() {
    return (
      <div>
        <NavLink to="/">home</NavLink>&nbsp;&nbsp;
        <NavLink to="/about">about</NavLink>&nbsp;&nbsp;
        <NavLink to="/dashoborad/10">dashboard</NavLink>&nbsp;&nbsp;
      </div>
    );
  }
}

export default Navi;
