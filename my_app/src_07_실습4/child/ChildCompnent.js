//React의 컴포넌트

import React, { Component } from 'react';

class ChildCompnent extends Component {

    constructor(props) {
        super(props);
        
        this.state= {
            num: 0,
            step: 1
        }

        this.handleEvent = this.handleEvent.bind(this);
        this.handleEvent2 = this.handleEvent2.bind(this);
    }
    
    handleEvent() {
        //state의 num값 변경

        //state의 값 변경은 다음과 같이 기존값에 새로운 값을 설정하는 형태로 작업해야한다.
        this.setState( {
            num: this.state.num + this.state.step
        })
    }

    handleEvent2() {                                           
        this.setState( {
            num: this.state.num - this.state.step > 0 ? this.state.num - this.state.step : 0 
        })        
    }

    stepChange = (e) => {
        this.setState( {
            step: Number.parseInt(e.target.value)
        })
    };

    render() {
        return (
            <div> 
                <h2>state실습</h2>
                step:
                <select onChange={this.stepChange} value={this.state.step}>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                </select><br/>
                num:{this.state.num}<br/>
                <button onClick={this.handleEvent}>+</button>                           
                <button onClick={this.handleEvent2}>-</button>                           
            </div>
        );
    }
}

export default ChildCompnent;