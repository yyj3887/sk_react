import ChildComponent from "./child/ChildComponent";
import First from "./child/First";
import Second from "./child/Second";

// 조건에 따라서 ChildCompnent에 전달할 컴포넌트가 달라진다. (First/Second)
function App() {
  let flag = true;
  return (
    <div>
      <ChildComponent>        
        {
            flag ? <First/> : <Second/>
        }
      </ChildComponent>
    </div>
  );
}

export default App;
