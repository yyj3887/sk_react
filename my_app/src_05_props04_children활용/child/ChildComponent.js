import React, { Component } from 'react';

class ChildComponent extends Component {
    render() {
        //1. props 사용
        var xxx = this.props;
        
        //2. destructuring 사용 (******)
        const { name , age , children} = this.props;

        return (
            <>
                <div>                    
                    body:{children}
                </div>                
            </>
        );
    }
}

export default ChildComponent;