//React의 컴포넌트

import React, { Component } from 'react';

class ChildCompnent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            num: [10]
        }
    }
    
    handleEvent = () => {

        //spread연산자 이용하여 새로운 배열 생성        
        this.setState( {
            num: [...this.state.num,10]
        })
    };


    render() {
        return (
            <div>
               <h2>배열 state 사용</h2>
               배열값: {String(this.state.num)}
               <br></br>
               <button onClick={this.handleEvent} >배열추가</button>
            </div>
        );
    }
}

export default ChildCompnent;