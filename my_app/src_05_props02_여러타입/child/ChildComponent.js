import React, { Component } from 'react';

class ChildComponent extends Component {
    render() {
        //1. props 사용
        var xxx = this.props;
        
        //2. destructuring 사용 (******)
        const { boolValue, numValue , arrayValue, objValue, nodeValue, funcValue } = this.props;

        return (
            <>
                <div>
                    boolValue : {String(boolValue) } <br/>
                    numValue : { numValue} <br/>
                    arrayValue : { arrayValue} <br/>
                    objValue : {String(objValue) } <br/>
                    nodeValue : {nodeValue } <br/>
                    funcValue : {String(funcValue)} <br/>                    
                </div>                
            </>
        );
    }
}

export default ChildComponent;