import ChildComponent from "./child/ChildComponent";

function App() {
  return (
    <div>
      <ChildComponent 
        boolValue={true}
        numValue={20}
        arrayValue={[10,20,30]}
        objValue={{v1:100,v2:200}}
        nodeValue={<h1>노드</h1>}
        funcValue={()=> {console.log("func")}}
      />
    </div>
  );
}

export default App;
