//React의 컴포넌트

import React, { Component } from 'react';
import axios from 'axios';


class ChildCompnent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            items:[]
        }
    }
    
    componentDidMount() {
        console.log("componentDidMount!!");
        axios.get("https://jsonplaceholder.typicode.com/users")
            .then( (result) => {                 
                this.setState( { 
                    items: result.data 
                });
            })
            .catch( (error)=> { console.log(error)});
    }
    

    render() {
        console.log("rendor!!");
        return (
            <div>     
                <ul>
                {
                    this.state.items.map((item,i) => {
                        return <li key={i}>{item.name}&nbsp;{item.email}</li>
                    })
                }
                </ul>                  
            </div>
        );
    }
}

export default ChildCompnent;