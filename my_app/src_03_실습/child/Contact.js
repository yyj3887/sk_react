import React, { Component } from 'react';

class Contact extends Component {
    render() {
        return (
            <div>
                <ul>
                    <li>홍길동</li>
                    <li>이순신</li>
                    <li>유관순</li>
                    <li>강감찬</li>                
                </ul>
            </div>
        );
    }
}

export default Contact;