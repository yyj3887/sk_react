import logo from './logo.svg';
import './App.css';
import ChildCompnent from './child/ChildCompnent';
import ContactList from './child/ContactList';

function App() {
  return (
    <div className="App">      
      <ContactList />
    </div>
  );
}

export default App;
