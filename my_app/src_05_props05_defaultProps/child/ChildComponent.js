import React, { Component } from 'react';

class ChildComponent extends Component {
    render() {
        //1. props 사용
        var xxx = this.props;
        
        //2. destructuring 사용 (******)
        const { name , age , children} = this.props;

        console.dir(ChildComponent);
        console.log(this);

        return (
            <>
                <div>
                    이름:{name}<br/>                    
                    나이:{age}<br/>                    
                </div>                
            </>
        );
    }
}

//default props값 설정
ChildComponent.defaultProps = {
    name:"유관순",
    age:100
}

export default ChildComponent;