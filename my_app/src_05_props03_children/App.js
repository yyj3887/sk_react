import ChildComponent from "./child/ChildComponent";

function App() {
  return (
    <div>
      <ChildComponent name="홍길동" age={20}>
        <h1>Hello</h1>
      </ChildComponent>

    </div>
  );
}

export default App;
