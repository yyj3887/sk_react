//React의 컴포넌트

import React, { Component } from 'react';

class ChildCompnent extends Component {

    constructor(props) {
        super(props);
        this.state = {userid: ''};
    
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
      }

      handleSubmit = (e) => {
        alert('A name was submitted: ' + this.state.userid);
        e.preventDefault(); //화면갱신을 막는다.
      }

      handleChange = (e) => {
        this.setState({
            userid: e.target.value
        });
      }
 
    render() {
        return (
            <div>                
                <form onSubmit={this.handleSubmit}>
                    <label>
                          userid:
                    <input type="text" value={this.state.userid} onChange={this.handleChange} />
                    </label>
                    <input type="submit" value="Submit" />
                </form>
            </div>
        );
    }
}

export default ChildCompnent;