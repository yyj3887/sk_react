import React, { Component } from 'react';
import Member from './Member';

class MemberList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            memberData: [],
            username:"",
            age:"",
            address:"",
        }
    }
    
    HandleSubmit = (e) => {
        e.preventDefault();

        let user = {
            username: this.state.username,
            age: this.state.age,
            address: this.state.address,
        }

        this.setState({
            memberData:[...this.state.memberData,user]
        })

    }

    HandleChange = (e) => {
        //동적 키 생성
        let nextState = {};
        nextState[e.target.name]=e.target.value;
        this.setState(nextState);
    }

    HandleEventName = (e) => {
        this.setState({
            username: e.target.value
        })
    }
    HandleEventAge = (e) => {
        this.setState({
            age:e.target.value
        })
    }
    HandleEventAddress = (e) => {
        this.setState({
            address:e.target.value
        })
    }

    render() {        
        return (
            <div>
                <form onSubmit={this.HandleSubmit}>
                    이름:<input type="text" name="username" onChange={ this.HandleChange } value={ this.state.username }/><br/>
                    나이:<input type="text" name="age" onChange={ this.HandleChange } value={ this.state.age } /><br/>
                    주소:<input type="text" name="address" onChange={ this.HandleChange } value={ this.state.address } /><br/>
                    <button type="submit">저장</button>
                </form>
                {
                    this.state.memberData.map( (row,index) => {
                        return <Member key={index} row={row}/>
                    })
                }                
            </div>
        );
    }
}

export default MemberList;