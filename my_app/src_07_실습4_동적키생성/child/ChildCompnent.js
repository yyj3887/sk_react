//React의 컴포넌트

import React, { Component } from 'react';

class ChildCompnent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            userid: '',
            num: []
        };

      }

      handleChange = (e) => {
        this.setState({
            userid: e.target.value
        });
      }

      add = (e) => {
          this.setState( {
              num:[...this.state.num,this.state.userid] //추가 주로 spread 연산을통하여 추가한다.
          })
          e.preventDefault(); //화면갱신을 막는다.
      }

      del = (e) => {
        this.setState( {
            num: this.state.num.filter( value => value != this.state.userid )//삭제 주로 filter함수를통하여 재정의한다.
        })
        e.preventDefault(); //화면갱신을 막는다.
    }
 
    render() {
        return (
            <div>                                                    
                userid: <input type="text" value={this.state.userid} onChange={this.handleChange} />    
                <button onClick={this.add}>추가</button>
                <button onClick={this.del}>삭제</button>
                배열값:{String(this.state.num)}
            </div>
        );
    }
}

export default ChildCompnent;