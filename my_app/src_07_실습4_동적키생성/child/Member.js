import React, { Component } from 'react';

class Member extends Component {
    render() {
        const {row , index } = this.props;
        return (
            <div>
                 <table border="1">
                    <thead>
                        <td>{row.username}</td>
                        <td>{row.age}</td>
                        <td>{row.address}</td>
                    </thead>           
                </table>                
            </div>
        );
    }
}

export default Member;