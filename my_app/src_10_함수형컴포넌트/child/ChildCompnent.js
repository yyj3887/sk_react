//React의 컴포넌트

import React, { Component } from 'react';

class ChildCompnent extends Component {
    render() {

        const {username,age} = this.props.user;       

        return (
            <div>
                이름:{username}<br/>         
                나이:{age}<br/>         
            </div>
        );
    }
}

export default ChildCompnent;