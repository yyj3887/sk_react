import React from 'react';

function ChildFunctionComponent(props) {
    const {username , age} = props.user;
    return (
        <div>
            이름:{username}<br/> 
            나이:{age}<br/> 
        </div>
    );
}

export default ChildFunctionComponent;