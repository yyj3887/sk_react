import React from 'react';

//(******)
const ChildArrowFunctionComponent = ({ user }) => {    
    return (
        <div>
            이름:{user.username}<br/> 
            나이:{user.age}<br/> 
        </div>
    );
}

export default ChildArrowFunctionComponent;