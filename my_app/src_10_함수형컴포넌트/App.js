import ChildArrowDestructuringFunctionComponent from './child/ChildArrowFunctionComponent';
import ChildCompnent from './child/ChildCompnent';
import ChildFunctionComponent from './child/ChildFunctionComponent';

function App() {

  const user = {
    username: "홍길동",
    age: 20
  }

  return (
    <div>
      <h1>1. 클래스형 컴포넌트</h1>
      <ChildCompnent user={user}/>
      <h1>2. 함수형 컴포넌트</h1>
      <ChildFunctionComponent user={user}/>
      <h1>3. arrow 함수형 컴포넌트</h1>
      <ChildFunctionComponent user={user}/>
      <h1>4. arrow + destructuring 함수형 컴포넌트</h1>
      <ChildArrowDestructuringFunctionComponent user={user}/>
    </div>
  );
}

export default App;
