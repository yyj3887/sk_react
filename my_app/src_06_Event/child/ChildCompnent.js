//React의 컴포넌트

import React, { Component } from 'react';

class ChildCompnent extends Component {

    constructor(props) {
        super(props);

        //이벤트 메서드 바인딩
        this.handleEvent4 = this.handleEvent4.bind(this);
    }
    

    //이벤트 처리 함수
    handleEvent(e) {
        console.log("Handle Evnet!!!",e);
    }

    //
    handleEvent2(x) {
        console.log("Handle Evnet!!!",x);
    }

    handleEvent3(x, e) {
        console.log("Handle Evnet!!!",x,e);
    }

    handleEvent4() {
        console.log("Handle Evnet!!!",this); // handleEvent4를 (생성자에서 처리)바인딩하면 this사용 가능
    }

    handleEvent5 = () => {
        console.log("handleEvent5",this) // arrow함수에서는 자동으로 this가 클래스 컴포넌트를 참조한다.
    }

    render() {
        return (
            <div>
                <h2>1. 인라인 형태의 arrow 함수 사용</h2>
                <button onClick={ () => {console.log("ok1")} }>OK1</button>
                <br></br>                
                <button onClick={ (e) => {console.log("ok1",e)} }>OK2</button>
                <br></br>                
                <h2>2. 임의의 함수 사용 (콜백형태로 전달 **함수이름만)</h2>
                <button onClick={this.handleEvent}>OK3</button>
                <br></br>                
                <h2>3. 임의의 함수 + 임의의 값 전달 </h2>
                <button onClick={ ()=> {this.handleEvent2("홍길동")} }>OK3</button>
                <br></br>                
                <h2>4. 임의의 함수 + 임의의 값 전달 + 이벤트객체</h2>
                <button onClick={ (e)=> {this.handleEvent3("홍길동",e)} }>OK4</button>
                <br></br>                
                <h2>5.  일반함수 + this </h2>
                <button onClick={ this.handleEvent4 }>OK5</button>
                <br></br>                
                <h2>6.  arrow 함수 + this </h2>
                <button onClick={ this.handleEvent5 }>OK6</button>

            </div>
        );
    }
}

export default ChildCompnent;