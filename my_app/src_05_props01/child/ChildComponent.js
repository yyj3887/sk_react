import React, { Component } from 'react';

class ChildComponent extends Component {
    render() {
        //1. props 사용
        var xxx = this.props;
        
        //2. destructuring 사용 (******)
        const { name, age } = this.props;

        return (
            <>
                <div>
                    이름:{xxx.name}<br/>
                    나이:{xxx.age}<br/>
                </div>
                <br/>
                <div>
                    이름:{name}<br/>
                    나이:{age}<br/>
                </div>
            </>
        );
    }
}

export default ChildComponent;