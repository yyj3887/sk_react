import ChildComponent from "./child/ChildComponent";

function App() {
  return (
    <div>
      <ChildComponent name="홍길동" age="20"/>
    </div>
  );
}

export default App;
