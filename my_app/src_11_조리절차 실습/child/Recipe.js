import React from 'react';

const Recipe= (props) => {
    return (
        <div>
            <h1>{props.name}</h1>
            <ul>
                {
                    props.ingredients.map((ingredient,i) => {
                        return <li key={i}>{ingredient.name}</li>
                    })
                }
            </ul>
            <h2>조리절차</h2>
            {
                props.steps.map( (step , i ) => {
                    return <p key={i}>{i+1}. {step}</p>
                })
            }
        </div>
    );
}

export default Recipe;