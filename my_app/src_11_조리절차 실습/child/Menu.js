import React from 'react';
import Recipe from './Recipe';

const Menu= (props) => {
    console.log(props)
    return (
        <div>
            <h1>{props.title}</h1>
            <div>
                {
                    props.recipes.map( (recipe,index) => {
                        return <Recipe 
                                    key={index}
                                    name={recipe.name}
                                    ingredients={recipe.ingredients}
                                    steps={recipe.steps} />
                    })
                }            
            </div>
        </div>
    );
}

export default Menu;