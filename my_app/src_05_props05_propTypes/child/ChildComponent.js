import React, { Component } from 'react';
import PropTypes from 'prop-types';

class ChildComponent extends Component {
    render() {
        //1. props 사용
        var xxx = this.props;
        
        //2. destructuring 사용 (******)
        const { boolValue, numValue , arrayValue, objValue, nodeValue, funcValue , oneOfValue } = this.props;

        return (
            <>
                <div>
                    boolValue : {String(boolValue) } <br/>
                    numValue : { numValue} <br/>
                    arrayValue : { arrayValue} <br/>
                    objValue : {String(objValue) } <br/>
                    nodeValue : {nodeValue } <br/>
                    funcValue : {String(funcValue)} <br/>                                        
                    oneOfValue : {oneOfValue}
                </div>                
            </>
        );
    }
}

//tpye checking
ChildComponent.propTypes = {
    boolValue: PropTypes.bool,
    numValue: PropTypes.number,
    arrayValue: PropTypes.array,
    objValue: PropTypes.object,
    nodeValue: PropTypes.node,
    funcValue: PropTypes.func,
    oneOfValue: PropTypes.oneOf(['남','여'])
}


export default ChildComponent;