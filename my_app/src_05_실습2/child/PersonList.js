import React, { Component } from 'react';

class PersonList extends Component {    
    render() {
        const {persons} = this.props;
        return (
            <div>
                <table border="1">
                    <thead>
                        <td>번호</td>
                        <td>이름</td>
                        <td>나이</td>
                    </thead>
                    <tbody>
                        {
                            persons.map((value,idex) => {
                                return <tr key={idex}>
                                        <td>{idex + 1}</td>
                                        <td>{value.name}</td>
                                        <td>{value.age}</td>
                                    </tr>
                            })
                        }
                    </tbody>
                </table>
            </div>
        );
    }
}

export default PersonList;