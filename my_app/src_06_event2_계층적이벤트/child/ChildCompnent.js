//React의 컴포넌트

import React, { Component } from 'react';
import SubChildComponent from './SubChildComponent';

class ChildCompnent extends Component {

    handleEvent() {
        console.log("handleEvent");        
    }

    handleEvent2(n) {
        console.log("handleEvent2",n);        
    }
    render() {
        return (
            <div>
                <SubChildComponent 
                onMyEvent={this.handleEvent} 
                onMyEvent2={this.handleEvent2} 

                />
            </div>
        );
    }
}

export default ChildCompnent;