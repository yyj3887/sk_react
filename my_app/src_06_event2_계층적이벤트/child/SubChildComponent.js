import React, { Component } from 'react';

class SubChildComponent extends Component {

    render() {

        const {onMyEvent , onMyEvent2} = this.props;

        return (
            <div>
                <button onClick={onMyEvent} >OK</button>
                <button onClick={() => {onMyEvent2("홍길동")}} >OK2</button>
            </div>
        );
    }
}

export default SubChildComponent;